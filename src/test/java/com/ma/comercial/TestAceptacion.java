package com.ma.comercial;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.ma.comercial.shared.mock.Mocks;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class TestAceptacion {

    protected static WireMockServer server;

    @BeforeAll
    static void config() {
        server = new WireMockServer(8180);
        server.start();
        Mocks.sucursales(server);
    }

    @AfterAll
    static void stop() {
        server.stop();
    }

}
