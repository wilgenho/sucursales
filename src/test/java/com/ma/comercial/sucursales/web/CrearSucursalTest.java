package com.ma.comercial.sucursales.web;


import com.intuit.karate.junit5.Karate;
import com.ma.comercial.TestAceptacion;

public class CrearSucursalTest extends TestAceptacion {

    @Karate.Test
    Karate deberiaCrearSucursales() {
        return Karate.run("classpath:karate/crear-sucursal.feature").tags("~@ignore");
    }

}
