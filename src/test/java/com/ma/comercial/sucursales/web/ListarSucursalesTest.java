package com.ma.comercial.sucursales.web;

import com.intuit.karate.junit5.Karate;
import com.ma.comercial.TestAceptacion;


public class ListarSucursalesTest extends TestAceptacion {

    @Karate.Test
    Karate deberiaListarSucursales() {
        return Karate.run("classpath:karate/listar-sucursales.feature").tags("~@ignore");
    }
}
