package com.ma.comercial.sucursales.web;

import com.intuit.karate.junit5.Karate;
import com.ma.comercial.TestAceptacion;

public class LoginUsuarioTest extends TestAceptacion {
    @Karate.Test
    Karate deberiaConsultarSucursal() {
        return Karate.run("classpath:karate/user-login.feature").tags("~@ignore");
    }

}
