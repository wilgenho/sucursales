package com.ma.comercial.sucursales.web;

import com.intuit.karate.junit5.Karate;
import com.ma.comercial.TestAceptacion;

public class ActualizarTelefonosTest extends TestAceptacion {

    @Karate.Test
    Karate deberiaActualizarTelefonos() {
        return Karate.run("classpath:karate/actualizar-telefonos.feature").tags("~@ignore");
    }

}
