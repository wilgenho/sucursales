package com.ma.comercial.sucursales.web;

import com.intuit.karate.junit5.Karate;
import com.ma.comercial.TestAceptacion;

public class BuscarSucursalesTest extends TestAceptacion {

    @Karate.Test
    Karate deberiaBuscarSucursales() {
        return Karate.run("classpath:karate/buscar-sucursales.feature").tags("~@ignore");
    }
}
