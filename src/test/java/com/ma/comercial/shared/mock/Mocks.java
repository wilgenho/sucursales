package com.ma.comercial.shared.mock;

import com.github.tomakehurst.wiremock.WireMockServer;
import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;

public class Mocks {

    private Mocks() {}

    /**
     *
     * @return sub para listar sucursales
     */

    public static void sucursales(WireMockServer server) {
        server.stubFor(
                get(urlEqualTo("/sucursales"))
                        .willReturn(aResponse()
                                .withStatus(200)
                                .withHeader("Content-Type", "application/json")
                                .withBodyFile("json/oficinas.json")));

        server.stubFor(
                get(urlEqualTo("/sucursales?q=san"))
                        .willReturn(aResponse()
                                .withStatus(200)
                                .withHeader("Content-Type", "application/json")
                                .withHeader("X-total-count", "4")
                                .withBodyFile("json/oficinas-busqueda.json")));

        server.stubFor(
                get(urlEqualTo("/sucursales?q=xxx"))
                        .willReturn(aResponse()
                                .withStatus(200)
                                .withHeader("Content-Type", "application/json")
                                .withHeader("X-total-count", "4")
                                .withBody("[]")));

        server.stubFor(
                get(urlEqualTo("/sucursales/101"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("json/oficina101.json")));

        server.stubFor(
                get(urlEqualTo("/sucursales/999"))
                .willReturn(aResponse()
                        .withStatus(404)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{ \"error\": \"No existe sucursal 999\" }")));

        server.stubFor(
                put(urlMatching("/sucursales/([0-9]*)/telefonos"))
                .withHeader("Authentication", equalTo("Bearer eZzWnreycP"))
                .willReturn(
                        aResponse()
                        .withStatus(204)));

        server.stubFor(
                put(urlMatching("/sucursales/([0-9]*)/telefonos"))
                        .withHeader("Authentication", absent())
                        .willReturn(
                                aResponse()
                                        .withStatus(401)));

        server.stubFor(
                post(urlEqualTo("/login"))
                .withRequestBody(containing("usuario=test"))
                .withRequestBody(containing("password=abc123"))
                .willReturn(aResponse()
                        .withStatus(201)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{ \"token\": \"eZzWnreycP\" }")));

        server.stubFor(
                post(urlEqualTo("/sucursales"))
                .withHeader("Authentication", equalTo("Bearer eZzWnreycP"))
                .willReturn(aResponse()
                .withStatus(201)));

        server.stubFor(
                post(urlEqualTo("/sucursales"))
               .withHeader("Authentication", equalTo("Bearer eZzWnreycP"))
               .withRequestBody(notMatching(".*subunidad.*"))
               .willReturn(aResponse()
               .withStatus(400)
               .withBody("{\"error\": \"Debe incluir subunidad\"}")));

        server.stubFor(
                post(urlEqualTo("/sucursales"))
                .withHeader("Authentication", absent())
                .willReturn(aResponse()
                .withStatus(401)));

    }

}
