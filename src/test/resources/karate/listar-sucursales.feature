#noinspection CucumberUndefinedStep
Feature: Listar sucursales

  Background:
    * url 'http://localhost:8180'

  Scenario: Listar todas las sucursales
    Given path 'sucursales'
    When method get
    Then status 200
    And match each response contains
    """
     {
        subunidad: '#number',
        nombre: '#string',
        email: '#string',
        telefonos: '#array'
     }
    """

    # Consultar datos de una sucursal dado el numero de subunidad obtenido en la lista
    Given def subunidad = response[0].subunidad
    And path 'sucursales' , subunidad
    When method get
    Then status 200
    And match response contains { subunidad: #(subunidad) }


