#noinspection CucumberUndefinedStep
Feature: Iniciar sesion de usuario

  Background:
    * url 'http://localhost:8180'

  Scenario: Obtener token de autenticación dado un usuario y contraseña
    Given path 'login'
    And form field usuario = 'test'
    And form field password = 'abc123'
    When method post
    Then status 201
