#noinspection CucumberUndefinedStep
Feature: Crear sucursales

  COMO administrador
  QUIERO crear una nueva sucursal
  PARA exponer los datos de contacto comercial


  Background:
    * url 'http://localhost:8180'
    * def token = 'Bearer eZzWnreycP'

  Scenario: Crear sucursal con datos válidos
    Given path 'sucursales'
    And header Content-Type = 'application/json'
    And header Authentication = token
    And request read('json/sucursal.json')
    When method post
    Then status 201

    Scenario: Crear sucursal con datos faltantes
      Given path 'sucursales'
      And header Content-Type = 'application/json'
      And header Authentication = token
      And request
    """
     {
       "nombre": "Mendoza",
       "direccion": "Necochea 183",
       "telefonos": ["(0261) 4298388"],
       "email": "reclamos.mdz@lamercantil.com.ar"
     }
    """
      When method post
      Then status 400


