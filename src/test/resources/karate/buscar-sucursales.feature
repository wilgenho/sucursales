#noinspection CucumberUndefinedStep
Feature: Buscar sucursal por nombre

  Background:
    * url 'http://localhost:8180'

  Scenario: Encontrar sucursales
    Given path 'sucursales'
    And param q = 'san'
    When method get
    Then status 200
    And match response == '#[2]'
    And match header X-total-count == '4'


  Scenario: No encontrar sucursales
    Given path 'sucursales'
    And param q = 'xxx'
    When method get
    Then status 200
    And match response == []

  Scenario Outline: Buscar sucursales
    Given path 'sucursales'
    And param q = '<texto>'
    When method get
    Then status 200
    And match $.length() == <cantidad>

    Examples:
    | texto | cantidad |
    | san   |    2     |
    | xxx   |    0     |


