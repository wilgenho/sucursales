#noinspection CucumberUndefinedStep
Feature: Actualizar teléfonos de sucursal

  Background:
    * url 'http://localhost:8180'
    * def telefonos = ['(0261) 4298388', '(0261) 4499912' , '(0261) 4499915' ]
    * def result = call read('user-login.feature')
    * def token = result.response.token

  Scenario: Actualizar telefonos con usuario autorizado
    Given path 'sucursales/101/telefonos'
    And header Authentication = 'Bearer ' + token
    And request telefonos
    When method put
    Then status 204

  Scenario: Actualizar telefonos con usuario autorizado
    Given path 'sucursales/101/telefonos'
    And request telefonos
    When method put
    Then status 401
