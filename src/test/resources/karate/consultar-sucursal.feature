#noinspection CucumberUndefinedStep
Feature: Consultar datos de una sucursal

  Background:
    * url 'http://localhost:8180'
    * def id = 101
    * def nombre = 'Mendoza'
    * def sucursal =
    """
    { 'subunidad' : #(id),
      'nombre': '#(nombre)',
      'direccion': 'Necochea 183',
      'telefonos': ['(0261) 4298388'],
      'email': 'reclamos.mdz@lamercantil.com.ar'
    }
    """

  Scenario: Consultar una sucursal existente
    Given path 'sucursales' , id
    When method get
    Then status 200
    And match response == sucursal

  Scenario: Consultar una sucursal que no existe
    Given path 'sucursales/999'
    When method get
    Then status 404
    And match response contains { error: '#string' }
    And match response contains { error: '#regex .+999$'}

